<?php

namespace Gpcrocker\Skeleton;

/**
 * Class Node
 * @package Gpcrocker\Skeleton
 */
class Node
{
    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $value;

    /**
     * @var Node
     */
    public $leftNode;

    /**
     * @var Node
     */
    public $rightNode;

    /**
     * Node constructor.
     * @param $key
     * @param $value
     */
    public  function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

}