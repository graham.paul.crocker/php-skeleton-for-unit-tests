<?php

namespace Gpcrocker\Skelton\Tests;

use PHPUnit\Framework\TestCase;

class SampleTest extends TestCase
{
    /**
     * @param array $data
     * @param array $expectedResult
     * @dataProvider dataProvideExample
     */
    public function testExampleFunction(array $data, array $expectedResult)
    {
        $data = [];
        $this->assertSame($data, $expectedResult);
    }

    /**
     * @return array[]
     */
    public function dataProvideExample(): array
    {
        return [
            'a case that is empty' => [
                'testA', [
                ]
            ],
            'a case that is singlar' => [
                'testB', [
                    [
                        'key' => 'value'
                    ]
                ]
            ],
            'a case that is multiple' => [
                'testC and testD', [
                    [
                        'key1' => 'value1'
                    ],
                    [
                        'key2' => 'value2'
                    ]
                ]
            ]
        ];
    }
}

