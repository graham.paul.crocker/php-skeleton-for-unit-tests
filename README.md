# Skeleton
 
## Description

Quickstart php skeleton for unit tests
 
## Installation
 
Run composer install
  
  `composer install`
  
## Executables
 
### Run PHPUnit Tests 
 
`chmod +x runTests && ./runTests`
  
### Run CLI

`php run.php`